-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Aug 12, 2021 at 07:47 AM
-- Server version: 5.7.32
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookmark`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `id_users` int(11) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `id_users`, `nom`, `description`) VALUES
(1, 1, 'Autodidacte', 'Ressources formation développeur web'),
(2, 1, 'Campus26', 'Ressources pertinentes'),
(4, 1, 'Frameworks', 'Bibliothèques'),
(11, 1, 'Langages', 'Langages de programmation'),
(15, 2, 'Bonus', 'coucou'),
(16, 2, 'Bonus2', 'Bonus2'),
(17, 8, '1ère categorie', 'Ouiii');

-- --------------------------------------------------------

--
-- Table structure for table `link`
--

CREATE TABLE `link` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `link`
--

INSERT INTO `link` (`id`, `id_user`, `nom`, `url`) VALUES
(1, 1, 'MDN Mozilla', 'https://developer.mozilla.org/en-US/docs/Web'),
(152, 1, 'Simploneline', 'https://simplonline.co/'),
(153, 1, 'GitLab', 'https://gitlab.com/'),
(156, 1, 'Google', 'http://google.fr'),
(166, 1, 'Openclassrooms', 'https://openclassrooms.com/fr/'),
(172, 1, 'jfejef', 'http://jbefjbe'),
(173, 2, 'Bonus', 'http://jejcejbn.fr'),
(174, 2, 'becjejc', 'http://hehcjjcebjc'),
(175, 2, 'ecece', 'http://hehcjjcebjc'),
(176, 2, '1', 'http://1'),
(178, 2, '2', 'http://hehcjjcebjc'),
(179, 8, 'Mon premier bookmark', 'http://ouii');

-- --------------------------------------------------------

--
-- Table structure for table `multi`
--

CREATE TABLE `multi` (
  `id_link` int(11) NOT NULL,
  `id_categories` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `multi`
--

INSERT INTO `multi` (`id_link`, `id_categories`) VALUES
(153, 2),
(156, 2),
(156, 4),
(166, 1),
(166, 4),
(166, 11),
(152, 1),
(152, 2),
(152, 4),
(1, 1),
(172, 1),
(172, 2),
(176, 15),
(178, 15),
(178, 16),
(179, 17);

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `username`, `email`, `password`) VALUES
(1, 'jocece', 'jnj@knck', '$2y$10$0rBrADL7qXXlycSGwXmPhOwMJdvKtcIyxnZQbmKx53maW5V5IhSWe'),
(2, 'guigui', 'guigui@gmail.com', '$2y$10$/UfSKWlTiWE5cWdNnlbGle8KSERQDDZoQD1jkOYFDTGtaH0cLvjLu'),
(3, 'jpp', 'jpp@gmail.com', '$2y$10$cVtIw2dmzvoGnnWSMY6KSOMPEzGwZ4KT.boLOwL.zEF0lJC6qivzK'),
(4, 'coucou', 'coucou@coucou.fr', '$2y$10$VGb2ASIduAP6iLYiQk3gKudDBn4kHvOK/o2VJmnVbWF7Yg0CwcrYO'),
(5, 'coucou5', 'coucou5@gmail.com', '$2y$10$DpZt6v5p2wPLrm27qwrR8O07TxnKatjPBCFazKOIxv1vEV//4REqW'),
(6, 'coucou6', 'coucou6@gmail.com', '$2y$10$AOHt7Hxk/R6aFZ0FPnp76uEB6Ubl.73LZ7J5UhVEnhIZLfzjWPRs2'),
(7, 'coucou7', 'coucou7@gmail.com', '$2y$10$kcnd1eN2mrwKJvDgS.IWc.cV4XhJdWKvKBlmiJSfon4oBN54Oo0sq'),
(8, 'guigui5', 'guigui5@gmail.com', '$2y$10$c7M9Kdjv04gxC0.T00ac4.pS0JcfTlN8QCL79Ua9r6WPM/Vng8hbK');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_users` (`id_users`);

--
-- Indexes for table `link`
--
ALTER TABLE `link`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `multi`
--
ALTER TABLE `multi`
  ADD KEY `contient` (`id_link`),
  ADD KEY `contient2` (`id_categories`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `link`
--
ALTER TABLE `link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`id_users`) REFERENCES `registration` (`id`);

--
-- Constraints for table `link`
--
ALTER TABLE `link`
  ADD CONSTRAINT `link_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `registration` (`id`);

--
-- Constraints for table `multi`
--
ALTER TABLE `multi`
  ADD CONSTRAINT `contient` FOREIGN KEY (`id_link`) REFERENCES `link` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contient2` FOREIGN KEY (`id_categories`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

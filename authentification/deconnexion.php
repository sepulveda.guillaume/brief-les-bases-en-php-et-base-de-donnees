<?php include('../index.html') ?>

<?php 
session_start();

// Suppression des variables de session et de la session
$_SESSION = array();
session_destroy();

// Suppression des cookies de connexion automatique
setcookie('username', '');
setcookie('password_hache', '');

echo '<div class="alert alert-success" role="alert">Vous êtes deconnecté !</div>';
echo '<form action="../index.php"><button type="submit" class="btn btn-dark ml-2">Accueil bookmarks</button></form>';

?>
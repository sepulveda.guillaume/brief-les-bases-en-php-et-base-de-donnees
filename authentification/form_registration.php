<?php include('../index.html') ?>

      <h1 class="mt-2 ml-2">Inscription</h1>
      <form name="register_form" method="post" action="registration.php" class="d-flex flex-column align-items-start">
         <label for="username" class="ml-2 mb-1">Username</label>
         <input type="text" class="ml-2" id="username" name="username" placeholder="Username" required>
         <label for="email" class="ml-2 mt-2 mb-1">E-mail</label>
         <input type="email" class="ml-2" id="email" name="email" placeholder="Email" required>
         <label for="password" class="ml-2 mt-2 mb-1">Mot de passe</label>
         <input type="password" class="ml-2" id="password" name="password" placeholder="Mot de passe" required>
         <label for="repass" class="ml-2 mt-2 mb-1">Retaper le mot de passe</label>
         <input type="password" class="ml-2" id="repass" name="repass" placeholder="Mot de passe" required>    
         <button type="submit" class="btn btn-primary mt-3 ml-2">M'inscrire</button>
      </form>
   </body>
</html>
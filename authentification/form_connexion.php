<?php include('../index.html') ?>

  <h1 class="mt-2 ml-2">Connexion</h1>
  <form name="connexion_form" method="post" action="connexion.php">
    <label for="username" class="ml-2">Username</label>
    <input type="text" class="ml-2" id="username" name="username" placeholder="Username" required>
    <label for="password" class="ml-2">Mot de passe</label>
    <input type="password" class="ml-2" id="password" name="password" placeholder="Mot de passe" required>
<!--<div> 
      <label for="auto">Connexion automatique</label>
      <input type="checkbox" name="auto_connexion" id="auto">
    </div> -->
    <button type="submit" class="btn btn-primary ml-2">Connexion</button>
  </form>
  <form name="inscription_form" method="post" action="form_registration.php">
    <button type="submit" class="btn btn-danger ml-2 mt-2">Inscription</button>
  </form>  
</body>
</html>
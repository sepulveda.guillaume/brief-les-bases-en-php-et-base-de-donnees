<?php 
if (isset($_SESSION['id']) AND isset($_SESSION['username']))
{
    echo '<div>';
        echo 'Bonjour <strong>' . $_SESSION['username'] . '</strong>';
    echo '</div>';
    echo '<div>';
        echo '<a href="./authentification/deconnexion.php">Deconnexion</a>';
    echo '</div>';
}
else {
    echo '<div>';
    echo '</div>';
    echo '<div class="m-0">';
        echo '<a href="./authentification/form_connexion.php" class="pr-2">Me connecter</a>';
        echo '<a href="./authentification/form_registration.php">M\'inscire</a>';
    echo '</div>';
}
?>
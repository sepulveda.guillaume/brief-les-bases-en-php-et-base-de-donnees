<?php include('../index.html') ?>

<?php include('../CRUD/bdd.php') ?>

<?php
   $username = $_POST["username"];
   $email = $_POST["email"];

   if ($_POST["password"] == $_POST["repass"]) {
      $password_hache = password_hash($_POST["password"], PASSWORD_DEFAULT);

      $only = $bdd->prepare("SELECT id FROM registration WHERE username = :username limit 1");
      $result_only = $only->execute(array(
         'username' => $username
      ));
      $only_reg = $only->fetchAll();
      if(count($only_reg)>0) {
         echo '<div class="alert alert-danger" role="alert">Le login que vous avez saisi existe déja !</div>';
         echo '<form action="./form_registration.php"><button type="submit" class="btn btn-dark ml-2">Retour inscription</button></form>';
      }
      else{
         $reg = $bdd->prepare("INSERT INTO registration(username, email, password) VALUES(:username, :email, :password)");
         $result_reg = $reg->execute(array(
            'username' => $username,
            'email' => $email,
            'password' => $password_hache
         ));
         if($result_reg) {
            echo '<div class="alert alert-success" role="alert">Votre inscription a bien été prise en compte !</div>';
            echo '<form action="../index.php"><button type="submit" class="btn btn-dark ml-2">Retour accueil</button></form>';
         }
         else {
         echo '<div class="alert alert-danger" role="alert">Erreur lors de votre inscription !</div>';
         echo '<form action="./form_registration.php"><button type="submit" class="btn btn-dark ml-2">Retour inscription</button></form>';
         }
      }
   }
   else {
      echo '<div class="alert alert-danger" role="alert">Mots de passe différents !</div>';
      echo '<form action="./form_registration.php"><button type="submit" class="btn btn-dark ml-2">Retour inscription</button></form>';
   }
?>
<?php include('../index.html') ?>

<?php include('../CRUD/bdd.php') ?>

<?php
    $username = $_POST["username"];
    $password = $_POST["password"];

    $connexion = $bdd->prepare("SELECT id, password FROM registration WHERE username = :username");
    $result_connexion = $connexion->execute(array(
        'username' => $username));
    $data_connexion = $connexion->fetch();

    // Comparaison du password envoyé via le formulaire avec la base

    if (!$data_connexion) {
        echo '<div class="alert alert-danger" role="alert">Mauvais identifiant ou mot de passe !</div>';
        echo '<form action="./form_connexion.php"><button type="submit" class="btn btn-dark ml-2">Retour connexion</button></form>';
    }
    else {
        $isPasswordCorrect = password_verify($_POST['password'], $data_connexion['password']);

        if ($isPasswordCorrect) {
        session_start();
           $_SESSION['id'] = $data_connexion['id'];
           $_SESSION['username'] = $username;
           echo '<div class="alert alert-success" role="alert">Vous êtes connecté !</div>';
           echo '<form action="../index.php"><button type="submit" class="btn btn-dark ml-2">Accueil bookmarks</button></form>';
        }
        else {
           echo '<div class="alert alert-danger" role="alert">Mauvais identifiant ou mot de passe !</div>';
           echo '<form action="./form_connexion.php"><button type="submit" class="btn btn-dark ml-2">Retour connexion</button></form>';
        }
   }
?>


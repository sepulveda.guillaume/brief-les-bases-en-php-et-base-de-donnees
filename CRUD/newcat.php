<?php include("bdd.php"); ?>

<?php include('../index.html') ?>

<?php session_start() ?>

<?php
	$nom_cat = $_POST['nom_cat'];
	$desc_cat = $_POST['desc_cat'];
	$id_users = $_SESSION['id'];

	$add_cat = $bdd->prepare("INSERT INTO categories (id_users, nom, description) VALUES(:id_users, :nom, :description)");
	$resultat_add = $add_cat->execute(array(
		'id_users' => $id_users,
		'nom' => $nom_cat,
		'description' => $desc_cat
		));

	if ($resultat_add) {
		echo '<div class="alert alert-success" role="alert">La catégorie a bien été crée !</div>';
	}
			
	else {
		echo '<div class="alert alert-danger" role="alert">Erreur lors de l\'ajout de la nouvelle catégorie !</div>';
	}
		
	echo '<form action="../index.php"><button type="submit" class="btn btn-dark ml-2">Retour bookmarks</button></form>';
?>		


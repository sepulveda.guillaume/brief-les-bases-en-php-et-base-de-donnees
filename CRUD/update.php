<?php include("bdd.php"); ?>

<?php include('../index.html') ?>

<?php session_start() ?>

    <form action="update_red.php" method="post" class="ml-2 mt-2">
        <h2>Modifie ton bookmark</h2>
        <div class="form_update d-flex flex-wrap align-items-center">
            <?php
            $id_update = $_POST['id_update'];

            $select = $bdd->prepare("SELECT link.id AS 'id', link.nom AS 'nom', link.url AS 'url', categories.id AS 'cat_id' FROM link inner join multi ON link.id = multi.id_link inner join categories ON categories.id = multi.id_categories WHERE link.id = :id_update");
            $select_execute = $select->execute(array(
                'id_update' => $id_update
            ));
            $resultat_select = $select->fetchAll();
            $idCats = [];
            foreach($resultat_select as $res) {
                array_push($idCats, $res['cat_id']);
            }
                echo '<input type="hidden" name="id_update" id="id_bookmark" value="' . $resultat_select[0]['id'] . '"</div>';
                echo '<input type="hidden" name="cat_update" id="cat_bookmark" value="' . $resultat_select[0]['cat_id'] . '"</div>';                    
                echo '<div class="form_update_nom"><label for="text_bookmark">Nom</label>';
                echo '<input type="text" class="ml-2 mr-3" name="nom" id="text_bookmark" value="' . $resultat_select[0]['nom'] . '" required></div>';
                echo '<div class="form_update_url"><label for="url_bookmark">URL</label>';
                echo '<input type="url" class="ml-2 mr-3" name="url" id="url_bookmark" value="' .$resultat_select[0]['url'] . '"required></div>';
            ?> 
            <div class="form_update_cat d-flex align-items-center">
                <label for="select_categories">Catégorie(s)</label>
                <select name="categories[]" id="select_categories" class="form-select  ml-2 mr-3 " multiple required>
                    <?php 
                    $id_users = $_SESSION['id'];

                    $reponse_cat = $bdd->prepare('SELECT categories.id, categories.nom FROM categories WHERE categories.id_users = :id_users');
                    $reponse_cat_execute = $reponse_cat->execute(array (
                        'id_users' => $id_users
                    ));
                    $donnees_cat = $reponse_cat->fetchAll();
                    foreach ($donnees_cat as $datas => $data) : 
                    ?>
                        <option class="ml-2 mr-2 mt-1" value="<?php echo $data['id']; ?>" for="select_categories" <?php if(in_array($data['id'], $idCats)) { echo "selected"; } ?>> <?php echo $data['nom'] ?> </option>
                    <?php 
                    endforeach;    
                    ?>      
                </select>   
                <button type="submit" class="btn btn-primary">Modifier</button>
            </div>
        </div>
    </form>
    <form action="../index.php"><button type="submit" class="btn btn-dark ml-2">Retour bookmarks</button></form>
</body>
</html>


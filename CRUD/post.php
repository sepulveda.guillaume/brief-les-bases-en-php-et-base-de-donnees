<?php include("bdd.php"); ?>

<?php include('../index.html') ?>

<?php session_start() ?>

<?php
	$nom = $_POST['nom'];
	$url = $_POST['url'];
	$id_user = $_SESSION['id'];

	$req = $bdd->prepare("INSERT INTO link (id_user, nom, url) VALUES(:id_user, :nom, :url)");
	$resultat = $req->execute(array(
		'id_user' => $id_user,
		'nom' => $nom,
		'url' => $url
		));

	$id_link = $bdd->lastInsertId();
	$id_categories = $_POST['categories'];
	$cat_id = "";

	$req_multi = $bdd->prepare("INSERT INTO multi (id_link, id_categories) VALUES(:id_link, :cat_id)");

	foreach ($id_categories as $cats => $cat) {
		$cat_id = $cat;

		$resultat_multi = $req_multi->execute(array(
		'id_link' => $id_link,
		'cat_id' => $cat_id
		));
	}

	if ($resultat_multi) {
		echo '<div class="alert alert-success" role="alert">Le bookmark a bien été ajouté dans la base de données !</div>';
	}
		
	else {
		echo '<div class="alert alert-danger" role="alert">Erreur lors de l\'ajout du bookmark !</div>';
	}

	echo '<form action="../index.php"><button type="submit" class="btn btn-dark ml-2">Retour bookmarks</button></form>';
?>
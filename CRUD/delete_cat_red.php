<?php include("bdd.php"); ?>

<?php include('../index.html') ?>

<?php session_start() ?>

<?php
    $cat_delete = $_POST['categories_delete'];

    $delete_cat = $bdd->prepare("DELETE FROM categories WHERE categories.id = :cat_delete");
    $resultat_delete_cat = $delete_cat->execute(array(
        'cat_delete' => $cat_delete
    ));

    if ($resultat_delete_cat) {
        echo '<div class="alert alert-success" role="alert">La catégorie a bien été supprimée de la base de données !</div>';
    }
    else {
        echo '<div class="alert alert-danger" role="alert">Erreur lors de la suppression de la ctagéorie !</div>';
    }
    
    echo '<form action="../index.php"><button type="submit" class="btn btn-dark ml-2">Retour bookmarks</button></form>';
?>
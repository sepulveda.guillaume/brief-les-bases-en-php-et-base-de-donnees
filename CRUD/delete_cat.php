<form action="./CRUD/delete_cat_red.php" method="post">
    <h2>Supprimer une catégorie</h2>
    <div class="row justify-content-center">
        <div class="col-4 d-flex flex-column">
            <label for="select_categories_delete">Catégorie</label>
            <select name="categories_delete" id="select_categories_delete" class="form-select" required>
                <?php 
                $id_users = $_SESSION['id'];

                $reponse_cat_delete = $bdd->prepare('SELECT categories.id, categories.nom FROM categories WHERE categories.id_users = :id_users');
                $reponse_cat__delete_execute = $reponse_cat_delete->execute(array (
                    'id_users' => $id_users
                ));
                $donnees_cat_delete = $reponse_cat_delete->fetchAll();
                foreach ($donnees_cat_delete as $cats_delete => $cat_delete ) {
                    echo '<option value="' . $cat_delete['id'] .'" for="select_categories">' . $cat_delete['nom'] . '</option>';
                }    
                ?>      
            </select>  
        </div>
    </div>
    <div class="button text-center m-2">
        <button type="submit" class="btn btn-danger my-2">Supprimer</button>
    </div>             
</form>

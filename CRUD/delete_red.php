<?php include("bdd.php"); ?>

<?php include('../index.html') ?>

<?php

$id_delete = $_POST['id_delete'];

$delete = $bdd->prepare("DELETE FROM link WHERE id = :id_delete");
$resultat_delete = $delete->execute(array(
    'id_delete' => $id_delete
));

if ($resultat_delete) {
    echo '<div class="alert alert-success" role="alert">Le bookmark a bien été supprimé de la base de données !</div>';
}
else {
    echo '<div class="alert alert-danger" role="alert">Erreur lors de la suppression du bookmark !</div>';
}

echo '<form action="../index.php"><button type="submit" class="btn btn-dark ml-2">Retour bookmarks</button></form>';

?>
<?php
  $tableau = $bdd->prepare('SELECT link.id AS "link_id", link.nom AS "link_name", link.url AS "link_url", GROUP_CONCAT(categories.nom ORDER BY categories.nom SEPARATOR " / ") AS "cat_name" , GROUP_CONCAT(categories.description ORDER by categories.nom SEPARATOR " / ") AS "cat_desc" FROM link left join multi ON link.id = multi.id_link left join categories ON categories.id = multi.id_categories WHERE link.id_user = :id_user AND categories.id_users = :id_user GROUP BY link.id');
  $tableau_execute = $tableau->execute(array(
    'id_user' => $_SESSION['id']
  ));

  // On affiche chaque entrée une à une
  $donnees_tableau = $tableau->fetchAll();
  foreach ($donnees_tableau as $datas => $data) {
    echo '<tr>';
      echo '<td class="align-middle">' . $data['link_name'] . '</td>';
      echo '<td class="align-middle">' . $data['link_url'] . '</td>';
      echo '<td class="align-middle">' . $data['cat_name'] . '</td>'; 
      echo '<td class="align-middle">' . $data['cat_desc'] . '</td>'; 
      echo '<td class="align-middle d-flex justify-content-center">';
      echo '<form action="./CRUD/update.php" method="post"><button type="button "class="btn btn-white" type="submit" name="id_update" value="' . $data['link_id'] . '"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
            </svg></button></form>';
      echo '<form action="./CRUD/delete.php" method="post"><button type="button "class="btn btn-white" type="submit" name="id_delete" value="' . $data['link_id'] . '"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
            </svg></button></form></td>';
    echo '</tr>';
    }
?>
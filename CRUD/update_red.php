<?php include("bdd.php"); ?>

<?php include('../index.html') ?>

<?php

$nom = $_POST['nom'];
$url = $_POST['url'];
$id_update = $_POST['id_update'];
        
$update = $bdd->prepare("UPDATE link SET link.nom = :nom, link.url = :url WHERE link.id = :id_update");
$resultat_update = $update->execute(array(
    'nom' => $nom,
    'url' => $url,
    'id_update' => $id_update
));

$cat_update = $_POST['cat_update'];

$multi_delete = $bdd->prepare("DELETE FROM multi WHERE id_link = $id_update");
$resultat_multi_delete = $multi_delete->execute();

$id_categories = $_POST['categories'];
$cat_id = "";

$req_multi_upd = $bdd->prepare("INSERT INTO multi (id_link, id_categories) VALUES(:id_update, :cat_id)");

foreach ($id_categories as $cats => $cat) {
	$cat_id = $cat;

	$resultat_multi_upd = $req_multi_upd->execute(array(
	'id_update' => $id_update,
	'cat_id' => $cat_id
	));
}

if ($resultat_multi_upd) {
    echo '<div class="alert alert-success" role="alert">Le bookmark a bien été modifié !</div>';
}
else {
    echo '<div class="alert alert-danger" role="alert">Erreur lors de la modification du bookmark !</div>';
}

echo '<form action="../index.php"><button type="submit" class="btn btn-dark ml-2">Retour bookmarks</button></form>';

?>
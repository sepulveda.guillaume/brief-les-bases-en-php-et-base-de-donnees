<form action="./CRUD/post.php" method="post">
    <h2>Ajouter un bookmark</h2>
    <div class="row ">
        <div class="col-4">
            <label for="text_bookmark" class="form-label">Nom</label>
            <input type="text" class="form-control" name="nom" id="text_bookmark" placeholder="Nom du bookmark" required>
        </div>
        <div class="col-4">
            <label for="url_bookmark" class="form-label">URL</label>
            <input type="url" class="form-control" name="url" id="url_bookmark" placeholder="URL du bookmark" required>
        </div>   
        <div class="col-4 d-flex flex-column ">
            <label for="select_categories">Catégorie</label>
            <select name="categories[]" id="select_categories" class="form-select" multiple required>
                <?php 
                $id_users = $_SESSION['id'];

                $reponse_cat = $bdd->prepare('SELECT categories.id, categories.nom FROM categories WHERE categories.id_users = :id_users');
                $reponse_cat_execute = $reponse_cat->execute(array (
                    'id_users' => $id_users
                ));
                $donnees_cat = $reponse_cat->fetchAll();
                foreach ($donnees_cat as $datas => $data) {
                    echo '<option class="ml-2 mt-1" value="' . $data['id'] .'" for="select_categories">' . $data['nom'] . '</option>';
                }    
                ?>      
            </select>  
        </div>            
    </div>
    <div class="button text-center m-2">
        <button type="submit" class="btn btn-dark my-2">Ajouter</button>
    </div>
</form>

<form action="CRUD/newcat.php" method="post">
    <h2>Ajouter une catégorie</h2>
    <div class="row ">
        <div class="col-4">
            <label for="newcat_name" class="form-label">Nom</label>
            <input type="text" class="form-control" name="nom_cat" id="newcat_name" placeholder="Nom de la catégorie" required>
        </div>
        <div class="col-8">
            <label for="newcat_desc" class="form-label">Nom</label>
            <input type="text" class="form-control" name="desc_cat" id="newcat_desc" placeholder="Description de la catégorie" required>
        </div>           
    </div>
    <div class="button text-center m-2">
        <button type="submit" class="btn btn-dark my-2">Ajouter</button>
    </div>
</form>

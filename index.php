<?php include('index.html') ?>

<?php include("./CRUD/bdd.php"); ?>

<?php session_start() ?>

    <div class="container w-50 py-3">
        <div class="row d-flex justify-content-between mx-0">
            <?php include('./authentification/connect_index.php') ?>
        </div>

        <?php 
        if (isset($_SESSION['id']) AND isset($_SESSION['username'])) {
        ?>

        <div class="row">
            <div class="col">
                <h1 class="text-center mt-3 mb-4">Mes bookmarks</h1>
            </div>
        </div>

        <table class="table table-hover table-striped text-center">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Nom</th>
                    <th scope="col">Url</th>
                    <th scope="col">Catégorie(s)</th>
                    <th scope="col">Description(s)</th>
                    <th scope="col">Edition</th>
                </tr>
            </thead>
            <tbody>
            <?php include("./CRUD/read.php"); ?>
            </tbody>
        </table>
        <hr>
        <?php include('./CRUD/add_link.php') ?>
        <hr>
        <?php include('./CRUD/add_cat.php') ?>
        <hr>
        <?php include('./CRUD/delete_cat.php') ?>
        <?php }
        else {} ?>
    </div>
</body>
</html>    